import React from 'react'
import PropTypes from 'prop-types'
import { pipe, replace } from 'ramda'
import styled from 'styled-components'
import * as API from 'constants/api'

const Content = styled('div')`
  & img {
    display: block;
    height: unset !important;
    max-width: 100%;
    width: unset !important;
  }
  
  & a {
    color: ${props => props.theme.colors.primary} !important;
    text-decoration: none !important;
  }

  & p {
    line-height: 20px;
  }

  & ul {
    & li {
      line-height: 20px;
    }
  }
  
  & table {
    border-collapse: collapse;
  }
`

const HtmlContent = props => {
  const { children } = props

  const formedContent = typeof children === 'string'
    ? pipe(
      replace(/\/media\//g, API.MEDIA_URL),
      replace(/<a/g, '<a target="_blank" rel="noopener noreferrer"')
    )(children)
    : children

  return (
    <Content dangerouslySetInnerHTML={{ __html: formedContent }} />
  )
}

HtmlContent.propTypes = {
  children: PropTypes.string
}

export default HtmlContent
