import React from 'react'
import PropTypes from 'prop-types'
import { DefaultToast } from 'react-toast-notifications'

const Toast = props => {
  const { transitionState } = props

  if (transitionState === 'exited') {
    return null
  }

  return (
    <DefaultToast {...props} />
  )
}

Toast.propTypes = {
  transitionState: PropTypes.string
}

export default Toast
