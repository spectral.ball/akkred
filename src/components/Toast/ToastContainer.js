import React from 'react'
import { DefaultToastContainer } from 'react-toast-notifications'

export default props => {
  return (
    <DefaultToastContainer
      {...props}
      style={{ overflow: 'hidden' }}
    />
  )
}
