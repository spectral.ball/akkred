import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'

export default styled('div')`
  border-radius: ${props => props.theme.borderRadius};
  border: ${props => props.theme.border};
  padding: 40px 25px;
  @media (min-width: ${devices.tabletL}) {
    padding: 50px;
  }
`
