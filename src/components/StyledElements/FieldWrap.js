import styled from 'styled-components'

export default styled('div')`
  &:not(:last-child) {
    margin-bottom: 30px;
  }
`
