import PropTypes from 'prop-types'
import styled from 'styled-components'

const Background = styled('div')`
  background-color: ${props => props.color};
  position: relative;
  z-index: 1;
  &:before {
    background-color: inherit;
    content: "";
    position: absolute;
    top: 0;
    bottom: 0;
    right: 100%;
    width: 100%;
    z-index: -1;
  }
  &:after {
    background-color: inherit;
    content: "";
    position: absolute;
    top: 0;
    bottom: 0;
    left: 100%;
    width: 100%;
    z-index: -1;
  }
`

Background.propTypes = {
  color: PropTypes.string,
}

Background.defaultProps = {
  color: 'white'
}

export default Background
