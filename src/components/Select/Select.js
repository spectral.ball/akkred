import React, { useRef, useState } from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'
import ReactSelect from 'react-select'
import getStyles from './getStyles'

const options = [
  { value: 'cherry', label: 'Cherry' },
  { value: 'banana', label: 'Banana' },
  { value: 'apple', label: 'Apple' }
]

const SelectWrapper = styled('div')``

const noOptionsMessage = ({ inputValue }) => {
  if (inputValue) return `Не найдено "${inputValue}"`
  return 'Не найдено'
}

const loadingMessage = ({ inputValue }) => {
  return 'Загрузка'
}

const Select = props => {
  const {
    theme,
    label,
    error,
    onChange,
    type,
    onMenuOpen,
    onMenuClose,
    height,
    isCreatable,
    onCreate,
    ...restProps
  } = props

  const selectRef = useRef()
  const [menuIsOpen, setMenuIsOpen] = useState(false)

  const handleMenuOpen = () => {
    setMenuIsOpen(true)
    if (typeof onMenuOpen === 'function') {
      onMenuOpen()
    }
  }

  const handleMenuClose = (ev) => {
    setMenuIsOpen(false)
    if (typeof onMenuClose === 'function') {
      onMenuClose()
    }
  }

  return (
    <SelectWrapper>
      <ReactSelect
        ref={selectRef}
        options={options}
        classNamePrefix={'select'}
        styles={getStyles(theme, {
          height,
          menuIsOpen
        })}
        closeMenuOnSelect={!restProps.isMulti}
        menuIsOpen={menuIsOpen}
        openMenuOnClick={type === 'select'}
        onMenuOpen={handleMenuOpen}
        onMenuClose={handleMenuClose}
        noOptionsMessage={noOptionsMessage}
        loadingMessage={loadingMessage}
        onChange={option => {
          if (typeof onChange === 'function') {
            onChange(option)
          }
        }}
        menuPortalTarget={document.body}
        {...restProps}
      />
    </SelectWrapper>
  )
}

Select.propTypes = {
  theme: PropTypes.object,
  label: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
  type: PropTypes.oneOf([
    'select',
    'autocomplete'
  ]),
  isMulti: PropTypes.bool,
  isCreatable: PropTypes.bool,
  options: PropTypes.array.isRequired,
  onMenuOpen: PropTypes.func,
  onMenuClose: PropTypes.func,
  onCreate: PropTypes.func,
  height: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  menuIsOpen: PropTypes.bool
}

Select.defaultProps = {
  type: 'select',
  placeholder: 'Выберите из списка',
  isMulti: false,
}

export default withTheme(Select)
