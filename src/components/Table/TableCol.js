import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { Col } from 'components/Grid'

const StyledCol = styled(Col)`
  font-weight: ${props => props.isBody ? 'normal' : '300'};
  ${props => props.align === 'right' && (
    css`
      text-align: right;
    `
  )}
`

const TableCol = props => {
  const { children, span, ...restProps } = props

  return (
    <StyledCol span={span} isTable={true} {...restProps}>
      {children}
    </StyledCol>
  )
}

TableCol.propTypes = {
  children: PropTypes.node,
  span: PropTypes.number,
  align: PropTypes.oneOf([
    'left',
    'right'
  ]),
}

TableCol.defaultProps = {
  align: 'left'
}

export default TableCol
