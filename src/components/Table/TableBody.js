import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Wrapper = styled('div')`
`

const TableBody = props => {
  const { children } = props

  const cloneRows = (child, key) => {
    return React.cloneElement(child, { key, isBody: true })
  }

  return (
    <Wrapper>
      {React.Children.map(children, cloneRows)}
    </Wrapper>
  )
}

TableBody.propTypes = {
  children: PropTypes.node,
}

export default TableBody
