import styled from 'styled-components'

export default styled('div')`
  align-items: center;
  background-color: #f2f2f2;
  border-radius: 5px;
  display: flex;
  font-size: 16px;
  justify-content: center;
  min-height: 30px;
  margin-top: 10px;
  padding: 5px 15px;
  text-align: center;
`
