import { createContext } from 'react'

export default createContext({
  gutter: 30
})
