import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'
import TableContext from './TableContext'

const Wrapper = styled('div')`
  color: ${props => props.theme.colors.grey2};
  overflow-x: auto;
  @media (min-width: ${devices.tabletL}) {
    overflow: unset;
  }
`

const TableOverflow = styled('div')`
  min-width: 800px;
  @media (min-width: ${devices.tabletL}) {
    min-width: unset;
  }
`

const Table = props => {
  const { children, gutter } = props

  const providerValue = {
    gutter
  }

  return (
    <TableContext.Provider value={providerValue}>
      <Wrapper>
        <TableOverflow>
          {children}
        </TableOverflow>
      </Wrapper>
    </TableContext.Provider>
  )
}

Table.propTypes = {
  children: PropTypes.node.isRequired,
  gutter: PropTypes.number,
}

Table.defaultProps = {
  gutter: 30
}

export default Table
