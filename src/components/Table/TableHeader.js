import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Row } from 'components/Grid'

const Wrapper = styled('div')`
  & ${Row} {
    margin-bottom: 5px;
    div {
      font-size: 1.2em;
      font-weight: 590;
    }
  }
`

const TableHeader = props => {
  const { children } = props

  return (
    <Wrapper>{children}</Wrapper>
  )
}

TableHeader.propTypes = {
  children: PropTypes.node
}

export default TableHeader
