import styled from 'styled-components'

export default styled('div')`
  padding: 0 15px;
  max-width: 1200px;
  margin: 0 auto;
`
