import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
  html, body {
    overflow-x: hidden;
  }
  body {
    box-sizing: border-box;
    color: ${props => props.theme.colors.text};
    font-size: 15px;
    font-family: "Roboto", sans-serif;
    line-height: 1.25;
    margin: 0;
    min-height: 100%;
    height: auto;
    width: 100%;
  }
  
  b, strong {
    font-weight: 500;
  }
`
