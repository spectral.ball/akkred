import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'
import Title from './Title'

export default styled(Title)`
  @media (min-width: ${devices.tabletL}) {
    font-size: 36px;
    margin-bottom: 50px;
  }
`
