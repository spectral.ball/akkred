import React from 'react'
import PropTypes from 'prop-types'
import Head from 'next/head'

const Akkred = 'Akkred.uz'
const DocumentTitle = props => {
  const { children } = props

  const title = children ? `${children} - ${Akkred}` : Akkred

  return (
    <Head>
      <title>{title}</title>
      <meta property="og:title" content={title} key="title" />
    </Head>
  )
}

DocumentTitle.propTypes = {
  children: PropTypes.string
}

export default DocumentTitle
