import NextI18NextInstance from '../../../i18n'

const { Link, Router, Trans, useTranslation } = NextI18NextInstance

export {
  Link,
  Router,
  Trans,
  useTranslation
}
