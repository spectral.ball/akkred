import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import truncateWords from 'helpers/truncateWords'
import Link from 'components/Link'

const StyledItem = styled('div')`
  
`

const StyledLink = styled(Link)`
  font-weight: normal;
  &:hover {
    color: ${props => props.theme.colors.blue};
  }
`

const BreadcrumbItem = props => {
  const { href, children } = props

  if (href) {
    return (
      <StyledLink href={href}>{children}</StyledLink>
    )
  }

  const truncated = truncateWords(children, 7)

  return (
    <StyledItem>
      {truncated}
    </StyledItem>
  )
}

BreadcrumbItem.propTypes = {
  href: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      pathname: PropTypes.string,
      query: PropTypes.object
    })
  ]),
  location: PropTypes.object,
  children: PropTypes.node
}

export default BreadcrumbItem
