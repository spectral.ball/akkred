import styled, { css } from 'styled-components'
import PropTypes from 'prop-types'
import { devices } from 'constants/mediaQueries'

const MAX_COL_SIZE = 24

const calculateFlexBasis = props => `${(props.span / MAX_COL_SIZE) * 100}%`

const Col = styled('div')`
  ${props => {
    if (props.span) {
      if (props.isTable) {
        return css`
          flex-basis: ${calculateFlexBasis};
        `
      }
      return css`
        flex-basis: 100%;
        width: 100%;
        @media (min-width: ${devices.laptopS}) {
          flex-basis: ${calculateFlexBasis};
          width: unset;
        }
      `
    }
  }}
`

Col.propTypes = {
  span: PropTypes.number
}

export default Col
