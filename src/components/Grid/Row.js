import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { devices } from 'constants/mediaQueries'

const Row = styled('div')`
  align-items: ${props => props.align};
  justify-content: ${props => props.justify};
  display: flex;
  flex-wrap: wrap;

  @media (min-width: ${devices.laptopS}) {
    flex-wrap: unset;
  }

  ${props => {
    const gutter = props.gutter
    const spaceValue = gutter / 2

    if (props.isTable && gutter) {
      return css`
        & > div {
          padding-left: ${spaceValue}px;
          padding-right: ${spaceValue}px;
        }
      `
    }

    return gutter && (
      css`
        & > div {
          @media (min-width: ${devices.laptopS}) {
            padding-left: ${spaceValue}px;
            padding-right: ${spaceValue}px;
          }
        }
      `
    )
  }}
`

Row.propTypes = {
  align: PropTypes.string,
  justify: PropTypes.string,
  gutter: PropTypes.number
}

export default Row
