import styled from 'styled-components'
import ButtonBase from './ButtonBase'

export default styled(ButtonBase)`
  color: ${props => props.theme.colors.primary};
`
