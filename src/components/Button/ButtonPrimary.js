import styled from 'styled-components'
import hexToRgb from 'helpers/hexToRgb'
import ButtonBase from './ButtonBase'

export default styled(ButtonBase)`
  background-color: ${props => hexToRgb(props.theme.colors.primary, '0.9')};
  color: white;
  font-size: 15px;
  height: 40px;
  padding: 0 40px;
  transition: ${props => props.theme.transition};
  &:active {
    background-color: ${props => props.theme.colors.primary};
  }
`
