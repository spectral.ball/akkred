import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { devices } from 'constants/mediaQueries'
import Spinner from 'components/Spinner'

const ButtonBase = styled('button')`
  align-items: center;
  background-color: ${props => props.theme.colors.grey};
  border: none;
  border-radius: 5px;
  cursor: pointer;
  display: inline-flex;
  justify-content: center; 
  font-size: 14px;
  height: 38px;
  padding: 0 30px;
  transition: ${props => props.theme.transition};
  
  ${props => props.fullWidth && (
    css`
      max-width: 300px;
      width: 100%;
      @media (min-width: ${devices.laptopS}) {
        max-width: unset;
      }
    `
  )}
  
  ${props => props.fullWidthMobile && (
    css`
      width: 100%;
      @media (min-width: ${devices.tabletS}) {
        width: unset;
      }
    `
  )}

  &:focus {
    outline: 0;
  }
  
  &:disabled {
    opacity: 0.7;
    pointer-events: none !important;
  }
`

const Loader = styled(Spinner)`
  margin-left: 5px;
`

const Button = props => {
  const { children, loading, disabled, ...restProps } = props

  const isDisabled = disabled || loading

  return (
    <ButtonBase disabled={isDisabled} {...restProps}>
      {children}
      {loading && (
        <Loader
          size={18}
          color={'currentColor'}
        />
      )}
    </ButtonBase>
  )
}

Button.propTypes = {
  children: PropTypes.node,
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  fullWidth: PropTypes.bool
}

export default Button
