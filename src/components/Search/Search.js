import React, { useState } from 'react'
import styled from 'styled-components'
import { Search } from 'react-feather'
import Input from '../Input/SearchInput'
import { Dropdown, DropdownMenu, DropdownToggle } from 'reactstrap'

const Wrapper = styled('div')`
  align-items: center;
  justify-content: between;
  flex-direction: column;
  display: flex;
  color: ${(props) => props.theme.colors.grey4};
`
const Flex = styled('div')`
  display: flex;
`
const InputWrapper = styled('div')`
  display: flex;
  width: 281px;
  padding: 0px 0.6rem;
`

export default () => {
  const [isOpen, setIsOpen] = useState(false)
  const [inputValue, setInputValue] = useState('')
  const [searchHelpOpen, setSearchHelpOpen] = useState(false)
  const [searchOpen, setSearchOpen] = useState(false)
  const handleChange = (e) => {
    const value = e.target.value
    if (value.length > 0) {
      setSearchHelpOpen(true)
    } else {
      setSearchHelpOpen(false)
    }
    setInputValue(value)
  }

  return (
    <Wrapper>
      <Dropdown isOpen={searchOpen} direction="bottom">
        <DropdownToggle
          style={{
            backgroundColor: 'transparent',
            color: 'grey',
            border: 'none',
          }}
        >
          <Search onClick={() => setSearchOpen(!searchOpen)} direction="bottom" />
        </DropdownToggle>
        <DropdownMenu className="mt-3">
          <InputWrapper>
            <Input
              isOpen={isOpen}
              setIsOpen={setIsOpen}
              onEnter={() => setIsOpen(!isOpen)}
              onChange={handleChange}
              placeholder="Поиск..."
            />
          </InputWrapper>
        </DropdownMenu>
      </Dropdown>
      {/* {searchOpen && (
        <Flex>
          <Dropdown isOpen={searchHelpOpen} direction="left">
            <DropdownMenu className="mt-5">
              <DropdownItem>Somethig is runnung</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Flex>
      )} */}
    </Wrapper>
  )
}
