import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled, { css } from 'styled-components'
import hexToRgb from 'helpers/hexToRgb'
import { useTranslation } from 'components/I18N'

const languages = [
  { id: 'ru', name: 'Рус' },
  { id: 'uz', name: 'O\'zb' },
  { id: 'en', name: 'Eng' },
]

const Wrapper = styled('div')`
  align-items: center;
  border-left: ${props => props.theme.border};
  border-right: ${props => props.theme.border};
  display: flex;
  line-height: 18px;
  padding: 0 16px;
`

const Lang = styled('div')`
  color: ${hexToRgb('#000000', '0.24')};
  cursor: pointer;
  &:not(:last-child) {
    margin-right: 16px;
  }
  ${props => props.isCurrent && (
    css`
      color: ${props => props.theme.colors.primary};
      pointer-events: none;
    `
  )}
`

const Languages = props => {
  const { as: WrapComponent } = props

  const [, i18n] = useTranslation()
  const currentLang = prop('language', i18n)

  return (
    <WrapComponent>
      {languages.map(lang => {
        const id = prop('id', lang)
        const name = prop('name', lang)
        const isCurrent = id === currentLang

        return (
          <Lang
            key={id}
            isCurrent={isCurrent}
            onClick={() => i18n.changeLanguage(id)}>
            {name}
          </Lang>
        )
      })}
    </WrapComponent>
  )
}

Languages.propTypes = {
  as: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.string,
    PropTypes.object,
  ])
}

Languages.defaultProps = {
  as: Wrapper
}

export default Languages
