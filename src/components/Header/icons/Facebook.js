/* eslint-disable */
import React from 'react'
import SVG from 'components/Svg'

export default props => {
  return (
    <SVG viewBox={'0 0 24 24'} {...props}>
      <g>
        <path d="M19.5,2h-15C3.1,2,2,3.1,2,4.5v15C2,20.9,3.1,22,4.5,22H12v-6.9H9.5V12H12V9.5c0-2.1,1.7-3.8,3.8-3.8h2.5v3.1H17 c-0.7,0-1.3-0.1-1.3,0.6V12h3.1l-1.3,3.1h-1.9V22h3.8c1.4,0,2.5-1.1,2.5-2.5v-15C22,3.1,20.9,2,19.5,2z"/>
      </g>
    </SVG>
  )
}
