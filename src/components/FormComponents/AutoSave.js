import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { head, keys, mapObjIndexed, pipe, prop } from 'ramda'
import styled from 'styled-components'
import { FormSpy } from 'react-final-form'
import objectDiff from 'helpers/objectDiff'

const LoaderWrap = styled('div')`
  background-color: ${props => props.theme.colors.primary};
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  height: 2px;
`

const stringifyValues = values => {
  return mapObjIndexed(JSON.stringify, values)
}

class AutoSave extends Component {
  state = {
    values: this.props.values,
    submitting: false
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.timeout) {
      clearTimeout(this.timeout)
    }
    this.timeout = setTimeout(this.save, this.props.debounce)
  }

  save = async () => {
    if (this.promise) {
      await this.promise
    }
    const { values, onFormChange } = this.props

    const strStateValues = stringifyValues(this.state.values)
    const strValues = stringifyValues(values)

    const difference = objectDiff(strStateValues, strValues)
    if (keys(difference).length) {
      this.setState({ submitting: true, values })
      const key = pipe(keys, head)(difference)
      this.promise = onFormChange({
        [key]: prop(key, values)
      })
      await this.promise
      delete this.promise
      this.setState({ submitting: false })
    }
  }

  render () {
    return this.state.submitting && (
      <LoaderWrap />
    )
  }
}

AutoSave.propTypes = {
  values: PropTypes.object,
  debounce: PropTypes.number,
  debounced: PropTypes.array,
  onFormChange: PropTypes.func.isRequired
}

AutoSave.defaultProps = {
  debounced: [],
  debounce: 1000
}

export default props => (
  <FormSpy
    {...props}
    subscription={{ values: true }}
    component={AutoSave}
  />
)
