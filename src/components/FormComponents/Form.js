import React from 'react'
import PropTypes from 'prop-types'
import { Form as FinalForm } from 'react-final-form'

const Form = props => {
  const {
    children,
    onSubmit,
    initialValues,
    resetOnSuccess,
    ...restProps
  } = props

  return (
    <FinalForm
      {...restProps}
      onSubmit={onSubmit}
      initialValues={initialValues}
      render={({ handleSubmit, form, ...formProps }) => {
        const onFormSubmit = event => {
          if (resetOnSuccess) {
            return handleSubmit(event).then((errors) => {
              if (!errors) {
                form.reset()
              }
            })
          }
          return handleSubmit(event)
        }

        return (
          <form onSubmit={onFormSubmit}>
            {React.cloneElement(children, formProps)}
          </form>
        )
      }}
    />
  )
}

Form.propTypes = {
  children: PropTypes.node.isRequired,
  onSubmit: PropTypes.func.isRequired,
  initialValues: PropTypes.object,
  resetOnSuccess: PropTypes.bool,
}

Form.defaultProps = {
  keepDirtyOnReinitialize: true,
  resetOnSuccess: false,
}

export default Form
