import React from 'react'
import PropTypes from 'prop-types'
import {
  getOptions,
  getOption,
  defaultGetText,
  defaultGetValue
} from './utils'
import SelectField from './SelectField'

const UniversalSelectField = props => {
  const { api, params, itemText, ...restProps } = props

  return (
    <SelectField
      getText={defaultGetText(itemText)}
      getValue={defaultGetValue(['id'])}
      getOptions={search => getOptions({ api, params, search })}
      getOption={getOption(api)}
      {...restProps}
    />
  )
}

UniversalSelectField.propTypes = {
  api: PropTypes.string.isRequired,
  params: PropTypes.object,
  itemText: PropTypes.array
}

UniversalSelectField.defaultProps = {
  itemText: ['title']
}

export default UniversalSelectField
