import React from 'react'
import PropTypes from 'prop-types'
import {
  getOptions,
  getOption,
  defaultGetText,
  defaultGetValue
} from './utils'
import SelectField from './SelectField'

const UniversalMultiSelectField = props => {
  const { api, params, itemText, ...restProps } = props

  return (
    <SelectField
      isMulti={true}
      getText={defaultGetText(itemText)}
      getValue={defaultGetValue(['id'])}
      getOptions={search => getOptions({ api, params, search })}
      getOption={getOption(api)}
      {...restProps}
    />
  )
}

UniversalMultiSelectField.propTypes = {
  api: PropTypes.string.isRequired,
  params: PropTypes.object,
  itemText: PropTypes.array
}

UniversalMultiSelectField.defaultProps = {
  itemText: ['name']
}

export default UniversalMultiSelectField
