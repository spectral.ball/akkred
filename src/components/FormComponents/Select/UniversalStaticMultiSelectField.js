import React from 'react'
import PropTypes from 'prop-types'
import {
  getStaticOption,
  getStaticOptions,
  defaultGetStaticText,
  defaultGetValue
} from './utils'
import { withTranslation } from 'hocs/withTranslation'
import SelectField from './SelectField'

const UniversalStaticMultiSelectField = props => {
  const { t, list, itemText } = props

  return (
    <SelectField
      getText={defaultGetStaticText(itemText, t)}
      getValue={defaultGetValue(['id'])}
      getOptions={search => getStaticOptions(search, list)}
      getOption={id => getStaticOption(id, list)}
      isStatic={true}
      isMulti={true}
      {...props}
    />
  )
}

UniversalStaticMultiSelectField.propTypes = {
  t: PropTypes.func.isRequired,
  list: PropTypes.array.isRequired,
  itemText: PropTypes.array
}

UniversalStaticMultiSelectField.defaultProps = {
  itemText: ['title']
}

export default withTranslation()(UniversalStaticMultiSelectField)
