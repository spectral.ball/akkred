import React from 'react'
import PropTypes from 'prop-types'
import { getFieldError } from 'helpers/form'
import { RadioGroup as RadioGroupUI } from 'components/Toggle'
import { Label, Error } from 'components/FormComponents'

const RadioGroup = props => {
  const { children, input, meta, label, ...restProps } = props

  const error = getFieldError(meta)

  return (
    <div>
      <Label error={error}>{label}</Label>
      <RadioGroupUI error={error} {...input} {...restProps}>
        {children}
      </RadioGroupUI>
      <Error>{error}</Error>
    </div>
  )
}

RadioGroup.propTypes = {
  children: PropTypes.node.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  label: PropTypes.string
}

export default RadioGroup
