import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import { CheckboxGroup as CheckboxGroupUI } from 'components/Toggle'

const CheckboxGroup = props => {
  const { children, input, ...restProps } = props
  const value = prop('value', input) || []

  return (
    <>
      <CheckboxGroupUI
        {...input}
        {...restProps}
        value={value}>
        {children}
      </CheckboxGroupUI>
    </>
  )
}

CheckboxGroup.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired
}

export default CheckboxGroup
