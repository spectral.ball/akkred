import React from 'react'
import PropTypes from 'prop-types'
import { getFieldError } from 'helpers/form'
import { Input } from 'components/Input'

const getInputValue = value => {
  if (!isNaN(value)) {
    return value
  }

  if (!value) {
    return ''
  }

  return value
}

const InputField = props => {
  const { input, meta, ...restProps } = props

  const value = input.value
  const error = getFieldError(meta)

  return (
    <Input
      {...input}
      {...restProps}
      value={getInputValue(value)}
      error={error}
    />
  )
}

InputField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
}

export default InputField
