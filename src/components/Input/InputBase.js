import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Input = styled('input')`
  background: ${props => props.theme.input.backgroundColor};
  border-radius: 6px;
  border: ${props => props.theme.border};
  color: inherit;
  font-size: 14px;
  font-family: inherit;
  outline: none;
  height: 36px;
  padding: 0 15px;
  transition: ${props => props.theme.transition};
  width: 100%;

  ::placeholder {
    color: ${props => props.theme.input.placeholderColor};
  }
  ::-ms-input-placeholder {
    color: ${props => props.theme.input.placeholderColor};
  }

  :hover {
    background: ${props => props.theme.input.backgroundColorHover};
  }

  :focus {
    background: ${props => props.theme.input.backgroundColor};;
    border-color: ${props => props.theme.colors.primary};
  }
`

const InputBase = props => {
  const { onKeyPress, onEnter, ...restProps } = props

  const onPress = event => {
    if (typeof onEnter === 'function' && event.key === 'Enter') {
      return onEnter(event, event.target.value)
    } else if (typeof onKeyPress === 'function') {
      return onKeyPress(event, event.target.value)
    }
    return null
  }

  return (
    <Input
      {...restProps}
      onKeyPress={onPress}
    />
  )
}

InputBase.propTypes = {
  onKeyPress: PropTypes.func,
  onEnter: PropTypes.func
}

export default InputBase
