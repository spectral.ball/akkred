import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Label } from 'components/FormComponents'
import InputBase from './InputBase'
import InputWrapper from './InputWrapper'

const StyledWrapper = styled(InputWrapper)`
  position: unset;
`

const InputIcon = icon => styled(icon)`
  color: ${props => props.theme.input.placeholderColor};
  font-size: 16px;
  position: absolute;
  top: 50%;
  left: 15px;
  transition: color 300ms, fill 300ms;
  transform: translateY(-50%);
  pointer-events: none;
`

const StyledInput = styled(props => <InputBase {...props} />)`
  padding-left: 40px;
  &:focus + svg {
    color: ${props => props.theme.colors.primary};
  }
`

const IconInput = ({ icon, label, ...props }) => {
  const Icon = InputIcon(icon)

  return (
    <StyledWrapper error={props.error}>
      <Label>{label}</Label>
      <InputWrapper>
        <StyledInput {...props} />
        <Icon size={16} />
      </InputWrapper>
    </StyledWrapper>
  )
}

IconInput.propTypes = {
  icon: PropTypes.func.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.any
}

export default IconInput
