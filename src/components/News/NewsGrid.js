import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'

export default styled('div')`
  display: grid;
  grid-gap: 40px;
  @media (min-width: ${devices.tabletL}) {
    grid-template-columns: repeat(3, 1fr);
  }
`
