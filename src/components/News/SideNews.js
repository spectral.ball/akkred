import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import { sprintf } from 'sprintf-js'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import * as ROUTES from 'constants/routes'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import AppContext from 'components/Contexts/AppContext'
import Title from 'components/Title'
import Button from 'components/Button'
import { TextOverflow, Datee } from 'components/StyledElements'

const Wrapper = styled('div')`
  border: ${props => props.theme.border};
  border-radius: ${props => props.theme.borderRadius};
  padding: 25px 20px;
`

const List = styled('div')`
  display: grid;
  grid-gap: 25px;
`

const News = styled('div')`
  
`

const NewsTitle = styled(TextOverflow)`
  font-size: 18px;
  line-height: 26px;
  margin-bottom: 5px;
`

const StyledButton = styled(Button)`
  margin-top: 12px;
`

const SideNews = props => {
  const { t } = props

  const router = useRouter()
  const { news } = useContext(AppContext)

  const goToDetail = id => () => {
    const path = ROUTES.NEWS_ITEM_PATH
    const asPath = sprintf(ROUTES.NEWS_ITEM_URL, id)
    return router.push(path, asPath)
  }

  return (
    <Wrapper>
      <Title>{t('news_title')}</Title>

      <List>
        {news.map(item => {
          const id = prop('id', item)
          const createdDate = prop('created_date_by_admin', item)
          const title = getTranslate(item, 'title')

          return (
            <News key={id}>
              <NewsTitle>{title}</NewsTitle>
              <Datee>{createdDate}</Datee>

              <StyledButton onClick={goToDetail(id)}>
                {t('more')}
              </StyledButton>
            </News>
          )
        })}
      </List>
    </Wrapper>
  )
}

SideNews.propTypes = {
  t: PropTypes.func.isRequired
}

export default withTranslation()(SideNews)
