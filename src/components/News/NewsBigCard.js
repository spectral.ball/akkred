import React from 'react'
import PropTypes from 'prop-types'
import { prop, pipe, slice } from 'ramda'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import * as ROUTES from 'constants/routes'
import getTranslate from 'helpers/getTranslate'
import extractContent from 'helpers/extractContent'
import { withTranslation } from 'hocs/withTranslation'
import { BackgroundImage, Datee, TextOverflow } from 'components/StyledElements'
import Button from 'components/Button'
import { sprintf } from 'sprintf-js'

const Wrapper = styled('div')`
  display: flex;
  margin-bottom: 50px;
`

const Image = styled(BackgroundImage)`
   background-color: ${props => props.theme.colors.grey};
   height: 330px;
   margin-right: 25px;
   min-width: 45%;
`

const Content = styled('div')`
  flex-grow: 1;
`

const Title = styled(TextOverflow)`
  font-size: 28px;
  margin-bottom: 15px;
`

const Text = styled(TextOverflow)`
  color: #4f4f4f;
  font-size: 18px;
  line-height: 25px;
  margin: 30px 0;
`

const StyledButton = styled(Button)`
  font-size: 20px;
  height: 50px;
`

const NewsBigCard = props => {
  const { t, data } = props

  const router = useRouter()

  const id = prop('id', data)
  const createdDate = prop('created_date_by_admin', data)
  const image = prop('image_main', data)
  const title = getTranslate(data, 'title')
  const text = getTranslate(data, 'text')
  const content = pipe(extractContent, slice(0, 255))(text)

  const goToDetail = () => {
    const path = ROUTES.NEWS_ITEM_PATH
    const asPath = sprintf(ROUTES.NEWS_ITEM_URL, id)
    return router.push(path, asPath)
  }

  return (
    <Wrapper>
      <Image url={image} />

      <Content>
        <Title>{title}</Title>
        <Datee>{createdDate}</Datee>

        <Text limit={4}>{content}</Text>

        <StyledButton onClick={goToDetail}>
          {t('more')}
        </StyledButton>
      </Content>
    </Wrapper>
  )
}

NewsBigCard.getInitialProps = async () => ({
  namespacesRequired: ['common']
})

NewsBigCard.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
}

export default withTranslation()(NewsBigCard)
