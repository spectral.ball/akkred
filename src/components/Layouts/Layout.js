import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'

export default styled('div')`
  background-color: white;
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  padding-top: 80px;
  @media (min-width: ${devices.laptopS}) {
    padding-top: 125px;
  }
`
