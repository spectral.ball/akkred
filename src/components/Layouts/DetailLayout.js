import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'
import Container from 'components/Container'
import Header from 'components/Header'
import Footer from 'components/Footer'
import { Row, Col } from 'components/Grid'
import { SideNews } from 'components/News'
import Layout from './Layout'

const MainContent = styled('div')`
  flex-grow: 1;
`

const Grid = styled(Row)`
  margin: 50px 0;
`

const Aside = styled(Col)`
  display: none;
  @media (min-width: ${devices.laptopS}) {
    display: block;
  }
`

const DetailLayout = props => {
  const { children } = props

  return (
    <Layout>
      <MainContent>
        <Header />

        <Container>
          <Grid gutter={60}>
            <Col span={16}>
              {children}
            </Col>

            <Aside span={8}>
              <SideNews />
            </Aside>
          </Grid>
        </Container>
      </MainContent>

      <Footer />
    </Layout>
  )
}

DetailLayout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default DetailLayout
