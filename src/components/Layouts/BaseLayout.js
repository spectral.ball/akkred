import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Container from 'components/Container'
import Header from 'components/Header'
import Footer from 'components/Footer'
import Layout from './Layout'

const MainContent = styled('div')`
  flex-grow: 1;
  & > ${Container} {
    margin-top: 50px;
    margin-bottom: 50px;
  }
`

const BaseLayout = props => {
  const { children, ...restProps } = props

  return (
    <Layout {...restProps}>
      <MainContent>
        <Header />

        <Container>
          {children}
        </Container>
      </MainContent>

      <Footer />
    </Layout>
  )
}

BaseLayout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default BaseLayout
