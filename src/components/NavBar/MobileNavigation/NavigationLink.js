import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { isEmpty, not } from 'ramda'
import styled, { css } from 'styled-components'
import { ChevronDown } from 'react-feather'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import Link from 'components/Link'
import getChildLinkParams from '../getChildLinkParams'

const titleStyles = {
  borderRadius: props => props.theme.borderRadius,
  lineHeight: '20px',
  padding: '10px 0'
}

const getPaddingByLevel = props => {
  if (props.level !== 0) {
    return '15px'
  }
}

const NavGroup = styled('div')`
  padding-left: ${getPaddingByLevel};
  position: relative;
  transition: all 100ms;
  ${props => (props.isParent && props.isOpen) && (
    css`
      &:before {
        background-color: ${props => props.theme.colors.grey};
        content: "";
        position: absolute;
        top: 0;
        left: -15px;
        right: -15px;
        bottom: 0;
      }
    `
  )}
`

const ChildWrap = styled('div')({
  ...titleStyles,
  alignItems: 'center',
  color: props => (props.isOpen && props.isParent) ? props.theme.colors.primary : props.theme.colors.text,
  display: 'flex',
  justifyContent: 'space-between',
  fontWeight: '500',
  position: 'relative',
  transition: 'all 100ms',
  '&:before': {
    backgroundColor: props => (props.isOpen && props.isParent) && props.theme.colors.grey,
    borderRadius: props => props.theme.borderRadius,
    content: props => (props.isOpen && props.isParent) && '""',
    position: 'absolute',
    top: '0',
    left: '-15px',
    right: '-15px',
    bottom: '0',
    zIndex: '-1'
  },
  '& > svg': {
    transition: 'all 100ms ease-out',
    transform: props => props.isOpen ? 'rotate(180deg)' : 'rotate(0deg)',
    height: '18px',
    marginLeft: '15px',
    minWidth: '18px',
    width: '18px'
  }
})

const ChildGroup = styled('div')`
  margin-top: 5px;
  padding-left: 15px;
  & ${NavGroup} {
    padding-left: 0;
  }
`

const StyledLink = styled(Link)({
  ...titleStyles,
  display: 'block',
  fontWeight: 'normal'
})

const NavigationLink = props => {
  const {
    t,
    url,
    external,
    children,
    level,
    isChild,
    onCloseMenu
  } = props

  const title = getTranslate(props, 'title')
  const isParent = level === 0
  const linkExtraProps = external ? { target: '_blank' } : {}
  const hasChildren = not(isEmpty(children))

  const [openChild, setOpenChild] = useState(false)
  const toggleOpen = () => setOpenChild(!openChild)

  if (hasChildren) {
    const childGroupStyle = { display: openChild ? 'block' : 'none' }
    return (
      <NavGroup level={level}>
        <ChildWrap
          isOpen={openChild}
          isParent={isParent}
          onClick={toggleOpen}>
          {title}
          <ChevronDown />
        </ChildWrap>
        <ChildGroup level={level} style={childGroupStyle}>
          {children.map((item, index) => {
            return (
              <NavigationLink
                {...item}
                t={t}
                key={index}
                level={level + 1}
                isChild={true}
                onCloseMenu={onCloseMenu}
              />
            )
          })}
        </ChildGroup>
      </NavGroup>
    )
  }

  if (isChild) {
    const { href, asHref } = getChildLinkParams(url, external)

    return (
      <StyledLink
        href={href}
        asHref={asHref}
        external={external}
        onClick={onCloseMenu}
        {...linkExtraProps}>
        {title}
      </StyledLink>
    )
  }

  return (
    <StyledLink
      href={url}
      external={external}
      onClick={onCloseMenu}
      {...linkExtraProps}>
      {title}
    </StyledLink>
  )
}

NavigationLink.propTypes = {
  t: PropTypes.func.isRequired,
  url: PropTypes.string,
  external: PropTypes.bool,
  children: PropTypes.array,
  level: PropTypes.number,
  isChild: PropTypes.bool,
  onCloseMenu: PropTypes.func,
}

NavigationLink.defaultProps = {
  level: 0,
  external: false,
  url: '',
  children: []
}
export default withTranslation()(NavigationLink)
