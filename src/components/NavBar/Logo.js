import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ROOT_PATH } from 'constants/routes'
import Link from 'components/Link'

const Wrapper = styled('div')`
  align-items: center;
  display: flex;
`

const StyledLogo = styled('div')`
  background-image: url('/logo.svg');
  background-repeat: no-repeat;
  background-size: contain;
  background-position: left center;
  height: 40px;
  width: 140px;
`

const LogoILAC = styled('div')`
  background-image: url('/logo-ilac.svg');
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  height: 50px;
  margin-left: 10px;
  width: 185px;
`

const Logo = props => {
  const { isHeader } = props
  const linkStyle = { display: 'block' }

  if (isHeader) {
    return (
      <Wrapper>
        <Link href={ROOT_PATH} style={linkStyle}>
          <StyledLogo />
        </Link>
        <Link href={'//ilac.org/'} style={linkStyle}>
          <LogoILAC />
        </Link>

      </Wrapper>
    )
  }

  return <StyledLogo />
}

Logo.propTypes = {
  isHeader: PropTypes.bool,
}

Logo.defaultProps = {
  isHeader: true
}

export default Logo
