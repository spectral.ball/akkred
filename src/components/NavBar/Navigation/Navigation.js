import React, { useContext } from 'react'
import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'
import AppContext from 'components/Contexts/AppContext'
import NavigationLink from './NavigationLink'

const Wrapper = styled('div')`
  align-items: center;
  display: none;
  height: 100%;
  margin-left: auto;
  position: relative;
  @media (min-width: ${devices.laptopM}) {
    display: flex;
  }
`

export default () => {
  const { menu } = useContext(AppContext)

  return (
    <Wrapper>
      {menu.map((item, index) => (
        <NavigationLink
          key={index}
          {...item}
        />
      ))}
    </Wrapper>
  )
}
