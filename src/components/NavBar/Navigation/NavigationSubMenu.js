import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const POS_LEFT = 'left'
const POS_RIGHT = 'right'

const Container = styled('div')`
  background-color: white;
  border-radius: ${props => props.theme.borderRadius};
  box-shadow: ${props => props.theme.boxShadow};
  color: ${props => props.theme.colors.text};
  padding: 5px;
  position: absolute;
  width: 380px;
  z-index: 90;
  & > * {
    display: block;
    font-size: 14px;
    font-weight: normal;
    margin-right: 0;
  }
`

const getStyles = (isTopLevel, position) => {
  if (isTopLevel) {
    return position === POS_LEFT
      ? { top: '100%', right: '0' }
      : { top: '100%', left: '0' }
  }
  return position === POS_LEFT
    ? { top: '0', right: '100%' }
    : { top: '0', left: '100%' }
}

class NavigationSubMenu extends React.Component {
  constructor (props) {
    super(props)
    this.subMenuRef = React.createRef()
    this.state = {
      position: POS_RIGHT
    }
  }

  componentDidMount () {
    const subMenu = this.subMenuRef.current
    const menuBounds = subMenu ? subMenu.getBoundingClientRect() : {}
    const fits =
      menuBounds.top >= 0 &&
      menuBounds.left >= 0 &&
      menuBounds.right <= (window.innerWidth || document.documentElement.clientWidth) &&
      menuBounds.bottom <= (window.innerHeight || document.documentElement.clientHeight)

    if (!fits) {
      this.setState({
        position: POS_LEFT
      })
    }
  }

  render () {
    const { children, isTopLevel, ...restProps } = this.props
    const { position } = this.state

    const styles = getStyles(isTopLevel, position)

    return (
      <Container
        ref={this.subMenuRef}
        style={styles}
        {...restProps}>
        {children}
      </Container>
    )
  }
}

NavigationSubMenu.propTypes = {
  children: PropTypes.node,
  isTopLevel: PropTypes.bool,
}

export default NavigationSubMenu
