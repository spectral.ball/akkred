import React from 'react'
import PropTypes from 'prop-types'
import { useRouter } from 'next/router'
import styled, { css } from 'styled-components'
import { ChevronLeft, ChevronRight } from 'react-feather'
import hexToRgb from 'helpers/hexToRgb'
import { replaceRouterQuery } from 'helpers/router'
import scrollToTop from 'helpers/scrollToTop'
import { LEFT_PAGE, RIGHT_PAGE } from './contsants'
import { getPages, getCurrentPage } from './utils'

const Wrapper = styled('div')`
  align-items: center;
  display: flex;
  justify-content: center;
  margin-top: 50px;
`

const Page = styled('div')`
  align-items: center;
  background-color: white;
  border: ${props => props.theme.borderLight};
  border-radius: 5px;
  color: ${props => props.theme.colors.primary};
  cursor: pointer;
  font-size: 16px;
  font-weight: 500;
  display: flex;
  justify-content: center;
  transition: ${props => props.theme.transition};
  height: 30px;
  min-width: 30px;
  padding: 0 5px;
  &:not(:last-child) {
    margin-right: 10px;
  }
  &:hover {
    background-color: ${props => props.theme.colors.grey};
  }
  ${props => props.isActive && (
    css`
      background-color: ${props => props.theme.colors.primary};
      border-color: transparent;
      color: white;
      pointer-events: none;
    `
  )}
`

const PageNav = styled(Page)`
  color: ${hexToRgb('#000000', '0.4')};
`

const Pagination = props => {
  const { totalRecords, pageLimit, pageNeighbours } = props

  const router = useRouter()

  const currentPage = getCurrentPage('page', router)
  const thisPageNeighbours = Math.max(0, Math.min(pageNeighbours, 2))
  const totalPages = Math.ceil(totalRecords / pageLimit)

  if (!totalRecords || totalPages === 1) {
    return null
  }

  const pages = getPages({
    totalPages,
    currentPage,
    pageNeighbours: thisPageNeighbours
  })

  const goToPage = page => {
    scrollToTop()
    replaceRouterQuery({ page }, router)
  }

  const handleClick = page => () => {
    goToPage(page)
  }

  const handleMoveLeft = () => {
    goToPage(currentPage - 1)
  }

  const handleMoveRight = () => {
    goToPage(currentPage + 1)
  }

  return (
    <Wrapper>
      {pages.map((page, index) => {
        if (page === LEFT_PAGE) {
          return (
            <PageNav key={index} onClick={handleMoveLeft}>
              <ChevronLeft size={14} />
            </PageNav>
          )
        }

        if (page === RIGHT_PAGE) {
          return (
            <PageNav key={index} onClick={handleMoveRight}>
              <ChevronRight size={14} />
            </PageNav>
          )
        }

        const isActivePage = currentPage === page

        return (
          <Page key={index} isActive={isActivePage} onClick={handleClick(page)}>
            {page}
          </Page>
        )
      })}
    </Wrapper>
  )
}

Pagination.propTypes = {
  totalRecords: PropTypes.number.isRequired,
  pageLimit: PropTypes.number,
  pageNeighbours: PropTypes.number
}

Pagination.defaultProps = {
  pageLimit: 8,
  pageNeighbours: 1
}

export default Pagination
