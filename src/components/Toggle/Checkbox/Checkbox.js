import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import ToggleContainer from '../ToggleContainer'
import ToggleInput from '../ToggleInput'
import CheckMark from '../CheckMark'

const StyledCheckMark = styled(CheckMark)`
  &:after {
    border-style: solid;
    border-color: ${props => props.theme.colors.primary};
    border-width: 0 2px 2px 0;
    height: 7px;
    transform: rotate(45deg) scale(0);
    left: 6px;
    top: 2px;
    width: 3px;
  }
  ${props => props.indeterminate && (
    css`
      border-color: ${props => props.theme.colors.primary};
      &:after {
        background: ${props => props.theme.colors.primary};
        border: none;
        opacity: 1;
        transform: scale(1);
        left: 4px;
        top: 7px;
        height: 2px;
        width: 8px;
      }
    `
  )}
`

const StyledInput = styled(ToggleInput)`
  :checked + ${StyledCheckMark}:after {
    transform: rotate(45deg) scale(1);
  }
`

const Container = styled(ToggleContainer)`
  ${props => !props.label && (
    css`
      margin-bottom: 0;
      width: 18px;
      height: 18px;
      padding-left: 18px;
    `
  )}
`

const Checkbox = props => {
  const { onChange, ...restProps } = props

  const onChecked = event => {
    if (typeof onChange === 'function') {
      onChange(event.target.checked, event)
    }
  }

  return (
    <Container disabled={props.disabled} label={props.label} {...restProps}>
      {props.label}
      <StyledInput {...restProps} onChange={onChecked} type={'checkbox'} />
      <StyledCheckMark indeterminate={props.indeterminate} />
    </Container>
  )
}

Checkbox.propTypes = {
  label: PropTypes.string,
  value: PropTypes.any,
  disabled: PropTypes.bool,
  indeterminate: PropTypes.bool,
  onChange: PropTypes.func,
  checked: PropTypes.bool
}

StyledCheckMark.propTypes = {
  error: PropTypes.bool
}

export default Checkbox
