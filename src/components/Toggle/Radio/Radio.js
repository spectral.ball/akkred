import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import ToggleContainer from '../ToggleContainer'
import ToggleInput from '../ToggleInput'
import CheckMark from '../CheckMark'

const RadioCheckMark = styled(CheckMark)`
  border-radius: 50%;
  &:after {
    background: ${props => props.theme.colors.primary};
    border-radius: 50%;
    transform: scale(0);
    top: 5px;
    left: 5px;
    height: 6px;
    width: 6px;
  }
`

const RadioContainer = styled(ToggleContainer)`
  ${props => props.type === 'inline' && (
    css`
      align-items: center;
      display: inline-flex;
      height: 30px;
      margin-bottom: 0;
      margin-right: 25px;
      & ${RadioCheckMark} {
        top: 4px;
      }
    `
  )}
`

const StyledInput = styled(ToggleInput)`
  &:checked + ${RadioCheckMark}:after {
    transform: scale(1);
  }
`

const Radio = props => {
  const { label, type, ...restProps } = props

  return (
    <RadioContainer
      checked={props.checked}
      disabled={props.disabled}
      type={type}>
      <span>{label}</span>
      <StyledInput {...restProps} type={'radio'} />
      <RadioCheckMark />
    </RadioContainer>
  )
}

Radio.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  type: PropTypes.oneOf(['block', 'inline']),
}

export default Radio
