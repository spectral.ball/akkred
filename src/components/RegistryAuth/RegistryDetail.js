import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import dateFormat from 'helpers/dateFormat'
import getTranslate from 'helpers/getTranslate'
import {
  Table,
  TableBody,
  TableRow,
  TableCol
} from 'components/Table'
import * as CONST from '../../constants/backend'

const Wrapper = styled('div')``

const SimpleCol = styled(TableCol)`
  text-align: left;
  &:first-child {
    padding-left: 32px;
  }
  &:last-child {
    padding-right: 32px;
  }
`

const ExtLink = styled('a')`
  color: ${props => props.theme.colors.primary};
  text-decoration: none;
`

const RegistryDetail = props => {
  const { t, data } = props

  const titleOrgan = prop('title_organ', data)
  const ministryOffice = prop('ministry_office', data)
  const authorizedDocument = prop('authorized_document', data)
  const reestr = prop('reestr', data)
  const confirmReestr = prop('confirm_reestr', data)
  const mandatoryDocument = prop('mandatory_document', data)
  const name_of_the_conformity_assessment_body = prop('name_of_the_conformity_assessment_body', data)
  const the_type_document_issued = prop('the_type_document_issued', data)
  const field_of_activity = prop('field_of_activity', data)
  const type = prop('type', data)
  const typeName = CONST.registryAuthtatus.object[type]
  var links = ''
  var number = ''
  if (type === 'accreditation') {
    links = `http://akkred.uz/reestr/${reestr.area}`
    number = `${reestr.number}`
  } else {
    links = `http://akkred.uz/reestr-confirm/${confirmReestr.area}`
    number = `${confirmReestr.number}`
  }
  return (
    <Wrapper>
      <Table gutter={32}>
        <TableBody>
          <TableRow>
            <SimpleCol span={10}>{'Yuridik shaxs nomi'}</SimpleCol>
            <SimpleCol span={14}>{titleOrgan}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{'Idoraviy bo\'ysinuvi'}</SimpleCol>
            <SimpleCol span={14}>{ministryOffice}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{'Vakolat berilgan huquqiy hujjat'}</SimpleCol>
            <SimpleCol span={14}>{authorizedDocument}</SimpleCol>
          </TableRow>

          <TableRow>
            <SimpleCol span={10}>{'Muvofiqligi tasdiqlanishi majburiyligini belgilovchi huququy hujjat'}</SimpleCol>
            <SimpleCol span={14}>{mandatoryDocument}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{'Muvofiqlikni baholash organi nomi'}</SimpleCol>
            <SimpleCol span={14}>{name_of_the_conformity_assessment_body}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{'Faoliyat sohasi'}</SimpleCol>
            <SimpleCol span={14}>{field_of_activity}</SimpleCol>
          </TableRow>

          <TableRow>
            <SimpleCol span={10}>{'Muvofiqlikni tasdiqlash uchun rasmiylashtiradigan hujjat turi'}</SimpleCol>
            <SimpleCol span={14}>{the_type_document_issued}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{'Baholash turi (akkreditatsiya/ma\'qullash) '}</SimpleCol>
            <SimpleCol span={14}>{typeName}</SimpleCol>
          </TableRow>

          <TableRow>
            <SimpleCol span={10}>{'Utganlik haqidagi reyestr raqami'}</SimpleCol>
            <SimpleCol span={14}>
              <ExtLink href={`${links}`} rel={'noopener noreferrer'} target={'_blank'}>
                {number}
              </ExtLink>
            </SimpleCol>
          </TableRow>

        </TableBody>
      </Table>
    </Wrapper>
  )
}

RegistryDetail.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
}

export default RegistryDetail
