export default (string = '', count = 1) => {
  const truncated = string.split(' ').splice(0, count).join(' ')

  if (truncated.length < string.length) {
    return `${truncated}...`
  }

  return truncated
}
