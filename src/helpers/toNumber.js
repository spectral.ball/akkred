import { replace } from 'ramda'
import numberWithoutSpaces from './numberWithoutSpaces'

export default function toNumber (value) {
  const parsedStr = Number(value)
  return isNaN(parsedStr) ? 0 : parsedStr
}

const normalize = value => {
  const replaced = replace(',', '.', String(value))
  const output = replaced.replace(/[^\d.-]/g, '')

  const splitted = output.split('.')
  if (splitted.length > 2) {
    const [first, ...others] = splitted
    return first + '.' + others.filter(item => item).join('')
  }

  return output.replace(/\B(?=(\d{9})+(?!\d))/g, ' ')
}

export const normalizeNumber = (maxValue) => (value) => {
  if (!value) return value

  const numValue = toNumber(value)
  if (numValue > maxValue && maxValue) {
    return normalize(maxValue)
  }

  return normalize(value)
}

export const fieldNormalizeParams = {
  format: normalizeNumber(),
  parse: numberWithoutSpaces,
  onFocus: event => event.target.select()
}
