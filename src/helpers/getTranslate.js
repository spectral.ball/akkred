import { prop, path } from 'ramda'
import NextI18Next from '../../i18n'

export default (obj, objName = 'name') => {
  const language = path(['i18n', 'language'], NextI18Next)
  const name = `${objName}_${language}`
  return prop(name, obj)
}
