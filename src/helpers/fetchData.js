import fetch from 'isomorphic-unfetch'
import { pipe, toPairs, fromPairs, filter, prop, equals, not } from 'ramda'
import { paramsToQueryString } from 'helpers/url'
import toSnakeCase from 'helpers/toSnakeCase'

export const getFilteredParams = params => pipe(
  toPairs,
  filter(
    pipe(
      prop(1),
      equals(undefined),
      not
    )
  ),
  fromPairs,
  toSnakeCase
)(params)

export default (api, params) => {
  const filteredParams = getFilteredParams(params)
  const query = paramsToQueryString(filteredParams)
  const options = { method: 'GET' }

  return fetch(api + query, options)
    .then(response => response.json())
    .catch(Promise.reject)
}
