import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import * as ROUTES from 'constants/routes'
import { devices } from 'constants/mediaQueries'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import { BackgroundImage } from 'components/StyledElements'

const List = styled('div')`
  display: grid;
  grid-gap: 50px;
`

const ListItem = styled('div')`
  display: flex;
`

const Photo = styled(BackgroundImage)`
  height: 175px;
  min-width: 140px;
  width: 140px;
`

const Main = styled('div')`
  flex-grow: 1;
  padding: 0 0 0 25px;
  @media (min-width: ${devices.tabletS}) {
    padding: 25px;
  }
`

const FullName = styled('div')`
  font-size: 18px;
  font-weight: 500;
  margin-bottom: 4px;
`

const Position = styled('div')`
  font-weight: 500;
  margin-bottom: 10px;
`

const ValuesGrid = styled('div')`
  display: grid;
  grid-gap: 5px;
`

const LabelValue = styled('div')`
  & span {
    color: ${props => props.theme.colors.grey4};
  }
`

const Management = props => {
  const { t, data } = props

  const list = prop('results', data) || []

  return (
    <DetailLayout>
      <DocumentTitle>{t('management_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('management_title')}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{t('management_title')}</Title>

      <List>
        {list.map(item => {
          const id = prop('id', item)
          const image = prop('image', item)
          const fullName = getTranslate(item)
          const position = getTranslate(item, 'position')
          const description = getTranslate(item, 'description')
          const email = prop('email', item)
          const phone = prop('phone', item)

          return (
            <ListItem key={id}>
              <Photo url={image} />
              <Main>
                <FullName>{fullName}</FullName>
                <Position>{position}</Position>

                <ValuesGrid>
                  <LabelValue>
                    {t('common_email_short')}: <span>{email}</span>
                  </LabelValue>
                  <LabelValue>
                    {t('common_phone')}: <span>{phone}</span>
                  </LabelValue>
                  <LabelValue>
                    {t('management_reception_days')}: <span>{description}</span>
                  </LabelValue>
                </ValuesGrid>
              </Main>
            </ListItem>
          )
        })}
      </List>
    </DetailLayout>
  )
}

Management.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired
}

export default withTranslation()(Management)
