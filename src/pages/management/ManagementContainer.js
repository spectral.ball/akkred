import React from 'react'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import Management from './Management'

const ManagementContainer = props => ((
  <Management {...props} />
))

ManagementContainer.getInitialProps = async () => {
  const params = { pageSize: 100 }
  const data = await fetchData(API.MANAGEMENT_LIST, params)

  return {
    data,
    namespacesRequired: ['common']
  }
}

export default ManagementContainer
