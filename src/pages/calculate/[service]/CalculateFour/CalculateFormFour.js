import React, { Fragment, useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { equals, head, includes, not, path, pipe, prop, toPairs } from 'ramda'
import styled from 'styled-components'
// import { useForm } from 'react-final-form'
import * as CONST from 'constants/backend'
import { devices } from 'constants/mediaQueries'
import { fieldNormalizeParams } from 'helpers/toNumber'
import { useToasts } from 'react-toast-notifications'
import { FieldWrap, FormWrapper } from 'components/StyledElements'
import { Field, UniversalStaticSelectField, AutoSave } from 'components/FormComponents'
import { InputField } from 'components/FormComponents/Basic'
import { ButtonPrimary } from 'components/Button'
import Select from '../../../../components/Select'
import Label from '../../../../components/FormComponents/Label'

// const FlexFieldWrap = styled(FieldWrap)`
//   @media (min-width: ${devices.tabletL}) {
//     align-items: ${props => props.alignItems};
//     display: flex;
//     & > div {
//       flex-grow: 1;
//       width: calc(50% - 15px);
//         &:first-child:not(:last-child) {
//           margin-right: 10px;
//         }
//         &:last-child:not(:first-child) {
//           margin-left: 10px;
//         }
//       }
//
//     & ${FieldWrap} {
//       margin-bottom: 0;
//     }
//   }
// `
//
// const fieldNames = {
//   type: 'type',
//   calculationType: 'calculation_type',
//   numND: 'numND',
//   num_test: 'num_test',
//   numStaff: 'numStaff',
//   numObj: 'numObj',
// }
//
// const getDisplayStyle = condition => ({
//   display: condition ? 'block' : 'none',
// })

const CalculateForm = props => {
  // const { t, values, loading } = props
  //
  // // const form = useForm()
  //
  // const type = path([fieldNames.type, 'id'], values)
  // const calculationType = path([fieldNames.calculationType, 'id'], values)
  // const numberInspection = prop(fieldNames.numberInspection, values)
  // const calcTypeClearingValues = ['inspection_control', 'actualization']
  // const NumTestClearingValues = ['inspection_control']
  // const NumNdClearingValues = ['inspection_control']
  // const NumObjClearingValues = ['expertise']
  // const NumStaffClearingValues = ['expertise']
  //
  // const showCalcType = not(includes(type, calcTypeClearingValues))
  // const showNumTest = includes(type, NumTestClearingValues)
  // const showNumNd = not(includes(type, NumNdClearingValues))
  // const showNumObj = includes(calculationType, NumObjClearingValues)
  // const showNumStaff = not(includes(calculationType, NumStaffClearingValues))
  //
  // const onFormChange = value => {
  //   const [fieldName, fieldValue] = pipe(toPairs, head)(value)
  //   const fieldValueId = prop('id', fieldValue)
  //   console.log(fieldValueId)
  //
  //   if (equals(fieldName, fieldNames.type) && includes(fieldValueId, calcTypeClearingValues)) {
  //     if (calculationType) {
  //       form.change(fieldNames.calculationType, '')
  //       console.log('calculationType')
  //     }
  //     if (numberInspection) {
  //       form.change(fieldNames.numberInspection, '')
  //       console.log('numberInspection')
  //
  //     }
  //   }
  // }

  const cross = {
    backgroundColor: 'transparent',
    border: '2px solid red',
    borderRadius: '8px',
    width: '40px',
    marginLeft: '10px'
  }
  const buttonStyle = {
    backgroundColor: ' rgba(10,44,183,0.9)',
    color: ' white',
    fontSize: '15px',
    height: '40px',
    padding: ' 0 40px',
    transition: 'all 300ms ease',
    border: 'none',
    borderRadius: ' 5px',
    margin: '10px 10px',
  }

  const inputStyle = {
    background: '#ffffff',
    borderRadius: '6px',
    border: '1px solid #e0e0e0',
    color: 'inherit',
    fontSize: '14px',
    fontFamily: 'inherit',
    outline: 'none',
    height: '36px',
    padding: '0 15px',
    transition: 'all 300ms ease',
    width: '100%',
  }
  const inBox = {
    borderRadius: '5px',
    border: '1px solid rgb(224, 224, 224)',
    padding: '50px',
  }
  const [firstSelect, setFirstSelect] = useState(null)
  const [in1, setIn1] = useState('')
  const [in2, setIn2] = useState('')
  const [secondSelect, setSecondSelect] = useState(null)
  const [inputsCount1, setInputsCount1] = useState([{ in3: '' }])
  const addElement = () => {
    setInputsCount1(inputsCount1.concat({
      in3: '',
    }))
    console.log(inputsCount1)
  }
  const removeElement = (item) => {
    setInputsCount1(inputsCount1.filter((abs, index) => index !== item))
  }
  const changeValue = (e, ind) => {
    setInputsCount1(inputsCount1.map((item, index) => {
      return index === ind ? {
        ...item,
        [e.target.name]: e.target.value,
      } : item
    }))
  }
  const more = []
  const { addToast } = useToasts()
  const [result, setresult] = useState(null)
  const in1Ref = useRef(null)
  const in2Ref = useRef(null)

  const prover2 = true

  const send = (e) => {
    e.preventDefault()
    setDis(true)
    setTimeout(() => { setDis(false), 4000 })
    inputsCount1?.map(item => {
      more.push(Number(item.in3))
    })

    let data

    if (firstSelect?.value === 'accreditation' && secondSelect?.value === 'expertise' || firstSelect?.value === 'expansion' && secondSelect?.value === 'expertise') {
      data = {
        type: firstSelect?.value,
        calculation_type: secondSelect?.value,
        numND: in1,
        numObj: in2,
      }
    } else if (firstSelect?.value === 'accreditation' && secondSelect?.value === 'site' || firstSelect?.value === 'expansion' && secondSelect?.value === 'site') {
      data = {
        type: firstSelect?.value,
        calculation_type: secondSelect?.value,
        numND: in1,
        numStaff: in2,
        tnved_count: more,
      }
    } else if (firstSelect?.value === 'inspection_control') {
      data = {
        type: firstSelect?.value,
        num_test: in1,
        tnved_count: more,
      }
    } else if (firstSelect?.value === 'actualization') {
      data = {
        type: firstSelect?.value,
        numND: in1,
        numStaff: in2,
      }
    }

    const body = JSON.stringify(data)
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body
    }
     fetch("https://old.akkred.uz:8081/main/calculation/calculation_four/",   options)
         .then(response => {
           return response.json()
         })
         .then(response => {
           setresult(response?.results?.sum)
           })
      .catch(response => {
        addToast('Ошибка', { appearance: 'error' })
      })
  }
  const [dis, setDis] = useState(false)
  function formatDollar (num) {
    var p = num.toFixed(2).split('.')
    return p[0].split('').reverse().reduce(function (acc, num, i, orig) {
      return num + (num != '-' && i && !(i % 3) ? ' ' : '') + acc
    }, '')
  }
  const clear = (e) => {
    setFirstSelect(e)
    setIn1('')
    setIn2('')
    setSecondSelect(null)
    setInputsCount1([{ in3: '' }])
  }
  const clear2 = (e) => {
    setSecondSelect(e)
    setIn1('')
    setIn2('')
    setInputsCount1([{ in3: '' }])
  }
  useEffect(() => {

  }, [])
  return (
    <>
      {/* <Fragment> */}
      {/*  <AutoSave */}
      {/*    debounce={200} */}
      {/*    onFormChange={onFormChange} */}
      {/*  /> */}
      {/*  <FormWrapper> */}
      {/*    <FlexFieldWrap> */}
      {/*      <FieldWrap> */}
      {/*        <Field */}
      {/*          name={fieldNames.type} */}
      {/*          component={UniversalStaticSelectField} */}
      {/*          label={t('calculate_rate_type')} */}
      {/*          list={CONST.calculateRateType.list} */}
      {/*        /> */}
      {/*      </FieldWrap> */}
      {/*      <FieldWrap style={getDisplayStyle(showCalcType)}> */}
      {/*        <Field */}
      {/*          name={fieldNames.calculationType} */}
      {/*          component={UniversalStaticSelectField} */}
      {/*          label={t('calculate_service_type')} */}
      {/*          list={CONST.calculateServiceType.list} */}
      {/*        /> */}
      {/*      </FieldWrap> */}
      {/*    </FlexFieldWrap> */}

      {/*    <FlexFieldWrap> */}
      {/*      <FieldWrap style={getDisplayStyle(showNumNd)}> */}
      {/*        <Field */}
      {/*          name={fieldNames.numND} */}
      {/*          component={InputField} */}
      {/*          label={'Количество нормативных документов на объекты сертификации согласно проекту Области аккредитации.'} */}
      {/*          type={'number'} */}
      {/*          {...fieldNormalizeParams} */}
      {/*        /> */}
      {/*      </FieldWrap> */}
      {/*    </FlexFieldWrap> */}
      {/*    <FlexFieldWrap style={getDisplayStyle(showNumTest)}> */}
      {/*      <FieldWrap> */}
      {/*        <Field */}
      {/*          name={fieldNames.num_test} */}
      {/*          component={InputField} */}
      {/*          label={'Количество комплектов документов работ по подтверждению соответствия за отчетный период'} */}
      {/*          type={'number'} */}
      {/*          {...fieldNormalizeParams} */}
      {/*        /> */}
      {/*      </FieldWrap> */}
      {/*    </FlexFieldWrap> */}

      {/*    <FlexFieldWrap> */}
      {/*      <FieldWrap style={getDisplayStyle(showNumObj)}> */}
      {/*        <Field */}
      {/*          name={fieldNames.numObj} */}
      {/*          component={InputField} */}
      {/*          label={t('calculate_numObj')} */}
      {/*          type={'number'} */}
      {/*          {...fieldNormalizeParams} */}
      {/*        /> */}
      {/*      </FieldWrap> */}
      {/*      <FieldWrap style={getDisplayStyle(showNumStaff)}> */}
      {/*        <Field */}
      {/*          name={fieldNames.numStaff} */}
      {/*          component={InputField} */}
      {/*          label={t('calculate_numStaff')} */}

      {/*          type={'number'} */}
      {/*          {...fieldNormalizeParams} */}
      {/*        /> */}
      {/*      </FieldWrap> */}

      {/*    </FlexFieldWrap> */}

      {/*    <ButtonPrimary loading={loading} fullWidthMobile={true}> */}
      {/*      {t('calculate_submit')} */}
      {/*    </ButtonPrimary> */}
      {/*  </FormWrapper> */}
      {/* </Fragment> */}

      <form onSubmit={(e) => send(e)} style={inBox}>
        <div className="row">
          {
            firstSelect?.value === 'actualization' || firstSelect?.value === 'inspection_control'
              ? <div className="col-md-12">
                <Label>Вид оценки</Label>
                <Select
                  options={CONST.calculateRateType.list?.map((item) => {
                    return {
                      value: item.id,
                      label: item.title,
                    }
                  })}
                  onChange={(e) => {
                    clear(e)
                  }}
                />
              </div>
              : <>
                <div className="col-md-6">
                  <Label>Вид оценки</Label>
                  <Select
                    options={CONST.calculateRateType.list?.map((item) => {
                      return {
                        value: item.id,
                        label: item.title,
                      }
                    })}
                    onChange={(e) => {
                      clear(e)
                    }}
                  />
                </div>
                <div className="col-md-6">
                  <Label>Вид услуги</Label>
                  <Select
                    options={CONST.calculateServiceType.list?.map((item) => {
                      return {
                        value: item.id,
                        label: item.title,
                      }
                    })}
                    onChange={(e) => {
                      clear2(e)
                    }}
                  />
                </div>

              </>
          }

        </div>
        {
          firstSelect?.value === 'accreditation' && secondSelect?.value === 'site' ||
          firstSelect?.value === 'actualization' ||
          firstSelect?.value === 'expansion' && secondSelect?.value === 'site' ||
          firstSelect === null || secondSelect === null || secondSelect === null && firstSelect !== 'actualization' || firstSelect === 'inspection_control'
            ? <div className="row">

              {
                firstSelect?.value === 'inspection_control'
                  ? <div className="col-md-12 mt-3">
                    <Label>Количество комплектов документов работ по подтверждению соответствия за отчетный период
                    </Label>
                    <input required={true} name="in1" type="number" value={in1} onChange={(e) => setIn1(e.target.value)} style={inputStyle}/>
                  </div>
                  : <div className="col-md-12 mt-3">
                    <Label>Количество нормативных документов на объекты сертификации согласно проекту Области
                      аккредитации.</Label>
                    <input required={true} name="in1" type="number" value={in1} onChange={(e) => setIn1(e.target.value)} style={inputStyle}/>
                  </div>
              }
              {
                firstSelect?.value !== 'inspection_control'

                  ? <div className="col-md-12 mt-3">
                    <Label>Количество персонала органа</Label>
                    <input required={true} name="in2" type="number" value={in2} onChange={(e) => setIn2(e.target.value)} style={inputStyle}/>
                  </div>
                  : ''
              }
              {
                firstSelect?.value !== 'actualization'
                  ? <>
                    <div className="col-md-12 mt-3">
                      <Label>Количество 6-значных кодов ТН ВЭД объектов сертификации согласно

                        {
                          firstSelect?.value === 'expansion' ? ' заявляемому расширению к ' : ' заявляемой '
                        }
                        ОА:</Label>
                    </div>
                    {
                      inputsCount1?.length !== 0 ? inputsCount1?.map((item, index) => (
                        <div className="col-md-12 mb-3 d-flex">
                          <input onChange={(e) => changeValue(e, index)}
                            name="in3"
                            type="number"
                            placeholder="1"
                            required={true}
                            style={inputStyle}/>
                          {
                            index !== 0

                              ? <button type="button" style={cross} onClick={() => removeElement(index)}>
                                <span className="text-danger">&#10006; </span>
                              </button>
                              : ''
                          }
                        </div>
                      ))
                        : ''
                    }
                    <button style={buttonStyle} className="d-inline w-auto mb-5" type="button"
                      onClick={addElement}>Добавить ещё одно направление
                    </button>
                  </>
                  : ''
              }
            </div>
            : firstSelect?.value === 'accreditation' && secondSelect?.value === 'expertise' ||
            firstSelect?.value === 'expansion' && secondSelect?.value === 'expertise'
              ? <div className="row">
                <div className="col-md-12 mt-3">
                  <Label>Количество нормативных документов на объекты сертификации согласно проекту Области
                    аккредитации.</Label>
                  <input required={true} name="in1" type="number" value={in1} onChange={(e) => setIn1(e.target.value)} style={inputStyle}/>
                </div>
                <div className="col-md-12 mt-3">
                  <Label>Количество наименований объектов сертификации согласно проекту Области аккредитации.</Label>
                  <input required={true} name="in2" type="number" value={in2} onChange={(e) => setIn2(e.target.value)} style={inputStyle}/>
                </div>
              </div>
              :
              // firstSelect?.value === 'inspection_control' ?
              //   <div className="row">
              //     <div className="col-md-12 mt-3">
              //       <Label>Количество нормативных документов на объекты сертификации согласно проекту Области
              //         аккредитации.</Label>
              //       <input  value={in1}  onChange={(e) => setIn1(e.target.value)} type="number" style={inputStyle}/>
              //     </div>
              //     <div className="col-md-12 mt-3">
              //       <Label>Количество наименований объектов сертификации согласно проекту Области аккредитации.</Label>
              //       <input  value={in2} onChange={(e) => setIn2(e.target.value)} type="number" style={inputStyle}/>
              //     </div>
              //     <div className="col-md-12 mt-3">
              //       <Label>Количество 6-значных кодов ТН ВЭД объектов сертификации согласно заявляемой ОА:</Label>
              //     </div>
              //     {
              //       inputsCount1?.length !== 0 ? inputsCount1?.map((item, index) => (
              //           <div className="col-md-12 mb-3">
              //             <input onChange={(e) => changeValue(e, index)}
              //                    name="in3"
              //                    type="number"
              //                    style={inputStyle}/>
              //           </div>
              //         ))
              //         :
              //         ''
              //     }
              //     <button style={buttonStyle} className="d-inline m-3 mt-2 w-auto" type="button"
              //             onClick={addElement}>Добавить ещё одно направление
              //     </button>
              //   </div>
              //   :
              ''

        }
        <button style={buttonStyle} type="submit"
          // onClick={send}
          disabled={dis} className="m-0 mt-4    " > Рассчитать стоимость
        </button>
      </form>
      {
        result
          ? <div className="result mt-5">
            <p className="fw-bolder mb-2">Результат по вашему запросу:</p>
            <p><span className="fw-bolder"> Примерная цена: </span>{formatDollar(result)} UZS</p>
            <p>Примерная цена указана без учета НДС</p>
          </div>
          : ''
      }
    </>
  )
}

CalculateForm.propTypes = {
  t: PropTypes.func.isRequired,
  values: PropTypes.object,
  loading: PropTypes.bool,
}

export default CalculateForm
