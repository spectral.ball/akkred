import React from 'react'
import { useRouter } from 'next/router'
import CalculateTestLabContainer from './CalculateTestLabContainer'
import CalculateTestLabTwoContainer from './CalculateTwo/CalculateTestLabTwoContainer'
import CalculateTestLabThreeContainer from './CalculateThree/CalculateTestLabThreeContainer'
import CalculateTestLabFourContainer from './CalculateFour/CalculateTestLabFourContainer'
import CalculateTestLabPoiContainer from './CalculatePoi/CalculateTestLabPoiContainer'
import CalculateTestLabOspersContainer from './CalculateOspers/CalculateTestLabOspersContainer'
import CalculateTestLabOssmContainer from './CalculateOssm/CalculateTestLabOssmContainer'
import CalculateTestLabOkContainer from './CalculateOk/CalculateTestLabOkContainer'
import { path } from 'ramda'

const CalculateService = props => {
  const router = useRouter()
  const service = path(['query', 'service'], router)

  switch (service) {
    case 'testing_lab': return (
      <CalculateTestLabContainer
        service={service}
        {...props}
      />
    )
    case 'galibration_laboratories': return (
      <CalculateTestLabTwoContainer
        service={service}
        {...props}
      />
    )
    case 'verification_laboratories': return (
      <CalculateTestLabThreeContainer
        service={service}
        {...props}
      />
    )
    case 'product_certification': return (
      <CalculateTestLabFourContainer
        service={service}
        {...props}
      />
    )
    case 'poi': return (
      <CalculateTestLabPoiContainer
        service={service}
        {...props}
      />
    )

    case 'ospers': return (
      <CalculateTestLabOspersContainer
        service={service}
        {...props}
      />
    )
    case 'ossm': return (
      <CalculateTestLabOssmContainer
        service={service}
        {...props}
      />
    )
    case 'ok': return (
      <CalculateTestLabOkContainer
        service={service}
        {...props}
      />
    )
    default: return <div />
  }
}

CalculateService.getInitialProps = async () => ({
  namespacesRequired: ['common']
})

export default CalculateService
