import React from 'react'
import Calculate from './Calculate'

const CalculateContainer = props => (
  <Calculate {...props} />
)

CalculateContainer.getInitialProps = async () => ({
  namespacesRequired: ['common']
})

export default CalculateContainer
