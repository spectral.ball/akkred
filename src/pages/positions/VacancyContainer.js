import React from 'react'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import Vacancy from './Vacancy'

const VacancyContainer = props => (
  <Vacancy {...props} />
)

VacancyContainer.getInitialProps = async () => {
  const params = { pageSize: 1000 }
  const data = await fetchData(API.POSITION_LIST, params)

  return {
    data,
    namespacesRequired: ['common']
  }
}

export default VacancyContainer
