import { useReducer } from 'react'
import { always, filter, find, fromPairs, mapObjIndexed, pathEq, pipe, slice } from 'ramda'
import reducer from 'helpers/reducer'

export default (fieldsList, initialValues) => {
  const initialState = pipe(
    fromPairs,
    mapObjIndexed(always(false))
  )(fieldsList)

  const [state, dispatch] = useReducer(reducer, initialState)

  const getFieldsList = (list, name) => {
    if (state[name]) {
      return list
    }
    const value = initialValues[name]
    const sliced = slice(0, 3, list)
    const active = find(pathEq(['props', 'value'], value), list)

    if (active) {
      const filteredSliced = filter(item => {
        return item.props.value !== value
      }, sliced)

      return [active, ...filteredSliced].filter(Boolean)
    }

    return sliced
  }

  return {
    state,
    dispatch,
    getFieldsList
  }
}
