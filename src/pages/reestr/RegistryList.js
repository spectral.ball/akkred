import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import { withTranslation } from 'hocs/withTranslation'
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCol
} from 'components/Table'
import { RegistryRow, RegistryDetail } from 'components/Registry'

const ListCounts = styled('div')`
  font-size: 18px;
  margin-top: 25px;
`

const Count = styled('span')`
  color: ${props => props.theme.colors.grey4};
  font-weight: 500;
`

const TableContainer = styled('div')`
  text-align: center;
`

const ClickableRow = styled(RegistryRow)`
  cursor: pointer;
`

const DetailWrapper = styled('div')`
  position: relative;
  margin: 20px 0;
  &:after {
    border-radius: ${props => props.theme.borderRadius};
    border: ${props => props.theme.border};
    box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    pointer-events: none;
  }
`

const RegistryList = props => {
  const { t, data, count } = props

  const [activeRegistry, setActiveRegistry] = useState(null)

  const onOpenRegistry = id => () => {
    setActiveRegistry(id)
  }

  const onCloseRegistry = () => {
    setActiveRegistry(null)
  }

  return (
    <Fragment>
      <TableContainer>
        <Table gutter={40}>
          <TableHeader>
            <TableRow>
              <TableCol span={6}>{t('registry_table_number')}</TableCol>
              <TableCol span={6}>{t('registry_table_legal')}</TableCol>
              <TableCol span={6}>{t('registry_detail_accreditation_date')}</TableCol>
              <TableCol span={6}>{t('registry_status')}</TableCol>
            </TableRow>
          </TableHeader>
          <TableBody>
            {data.map(item => {
              const id = prop('id', item)
              const isActive = activeRegistry === id

              if (isActive) {
                const rowStyle = {
                  backgroundColor: 'white',
                  margin: '10px 0'
                }

                return (
                  <DetailWrapper key={id}>
                    <ClickableRow
                      t={t}
                      key={id}
                      data={item}
                      onClick={onCloseRegistry}
                      style={rowStyle}
                      isBody={true}
                    />

                    <RegistryDetail t={t} data={item} />
                  </DetailWrapper>
                )
              }

              return (
                <ClickableRow
                  t={t}
                  key={id}
                  data={item}
                  onClick={onOpenRegistry(id)}
                />
              )
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <ListCounts>
        {t('registry_query_count')}: <Count>{count}</Count>
      </ListCounts>
    </Fragment>
  )
}

RegistryList.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.array,
  count: PropTypes.number,
}

RegistryList.defaultProps = {
  data: [],
  count: 0
}

export default withTranslation()(RegistryList)
