import React from 'react'
import { prop } from 'ramda'
import { sprintf } from 'sprintf-js'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import DocumentsDetail from './DocumentsDetail'

const DocumentsDetailContainer = props => (
  <DocumentsDetail {...props} />
)

DocumentsDetailContainer.getInitialProps = async ({ query }) => {
  const id = prop('id', query)
  const data = await fetchData(sprintf(API.DOCUMENT_ITEM, id))

  return {
    data,
    namespacesRequired: ['common']
  }
}

export default DocumentsDetailContainer
