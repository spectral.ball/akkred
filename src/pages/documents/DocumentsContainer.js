import React from 'react'
import { prop } from 'ramda'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import Documents from './Documents'

const DocumentsContainer = props => (
  <Documents {...props} />
)

DocumentsContainer.getInitialProps = async ({ query }) => {
  const parents = prop('parents', query)
  const params = {
    parents,
    pageSize: 1000
  }
  const docData = await fetchData(API.DOCUMENT_LIST, params)

  return {
    docData,
    namespacesRequired: ['common']
  }
}

export default DocumentsContainer
