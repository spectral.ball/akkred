import React from 'react'
import { prop } from 'ramda'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import News from './News'

const NewsContainer = props => (
  <News {...props} />
)

NewsContainer.getInitialProps = async ({ query }) => {
  const page = prop('page', query)
  const newsData = await fetchData(API.NEWS_LIST, { page, pageSize: 12 })

  return {
    newsData,
    namespacesRequired: ['common']
  }
}

export default NewsContainer
