import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import * as ROUTES from 'constants/routes'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { BaseLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import { TitleBig } from 'components/Title'
import { Row, Col } from 'components/Grid'
import Pagination from 'components/Pagination'
import RegistryList from './RegistryList'
import RegistryFilter from './RegistryFilter'

const Registry = props => {
  const {
    t,
    registryData,
    regionData,
    organTypeData,
    onFormChange,
    onFormReset,
    initialValues
  } = props

  const registryCount = prop('count', registryData)
  const registryList = prop('results', registryData)

  const regionList = prop('results', regionData) || []
  const organTypeList = prop('results', organTypeData) || []

  return (
    <BaseLayout>
      <DocumentTitle>{t('registry_confirm_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('registry_confirm_title')}</BreadcrumbItem>
      </Breadcrumb>

      <TitleBig>{t('registry_confirm_title')}</TitleBig>

      <Row>
        <Col span={8}>
          <RegistryFilter
            t={t}
            initialValues={initialValues}
            onFormChange={onFormChange}
            onFormReset={onFormReset}
            regionList={regionList}
            organTypeList={organTypeList}
          />
        </Col>
        <Col span={16}>
          <RegistryList data={registryList} count={registryCount} />
          <Pagination totalRecords={registryCount} />
        </Col>
      </Row>
    </BaseLayout>
  )
}

Registry.propTypes = {
  t: PropTypes.func.isRequired,
  onFormChange: PropTypes.func.isRequired,
  onFormReset: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  registryData: PropTypes.object.isRequired,
  regionData: PropTypes.object.isRequired,
  organTypeData: PropTypes.object.isRequired,
}

export default withTranslation()(Registry)
