import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import * as ROUTES from 'constants/routes'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import {
  Form,
  Field,
  InputField,
  UniversalSelectField,
} from 'components/FormComponents'
import { FormWrapper, FieldWrap } from 'components/StyledElements'
import { ButtonPrimary } from 'components/Button'
import * as API from '../../constants/api'

const SatisfactionRating = props => {
  const { t, onSubmit, loading } = props

  return (
    <DetailLayout>
      <DocumentTitle>{t('request_course')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('request_course')}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{t('request_course')}</Title>

      <Form
        onSubmit={onSubmit}
        resetOnSuccess={true}
        keepDirtyOnReinitialize={false}>
        <Fragment>
          <FormWrapper>

            <FieldWrap>
              <Field
                name={'first_name'}
                component={InputField}
                label={t('course_first_name')}
              />
            </FieldWrap>

            <FieldWrap>
              <Field
                name={'last_name'}
                component={InputField}
                label={t('course_last_name')}
              />
            </FieldWrap>

            <FieldWrap>
              <Field
                name={'middle_name'}
                component={InputField}
                label={t('course_middle_name')}
              />
            </FieldWrap>

            <FieldWrap>
              <Field
                name={'organization'}
                component={InputField}
                label={t('course_organization')}

              />
            </FieldWrap>

            {/* 1 */}
            <FieldWrap>
              <Field
                name={'region'}
                component={UniversalSelectField}
                label={t('course_region')}

                api={API.REGION_LIST}
                params={{ type: 'local' }}
              />
            </FieldWrap>

            <FieldWrap>
              <Field
                name={'mail'}
                component={InputField}
                label={t('course_mail')}

              />
            </FieldWrap>

            <FieldWrap>
              <Field
                name={'phone'}
                component={InputField}
                label={t('course_phone')}

              />
            </FieldWrap>

            <FieldWrap>
              <Field
                name={'course'}
                component={UniversalSelectField}
                label={t('course_course')}
                api={API.COURSE_LIST}
              />
            </FieldWrap>

            <FieldWrap>
              <Field
                name={'comments'}
                component={InputField}
                label={t('course_comments')}

              />
            </FieldWrap>

            <FieldWrap>
              <ButtonPrimary loading={loading}>
                Отправить ответ
              </ButtonPrimary>
            </FieldWrap>
          </FormWrapper>
        </Fragment>
      </Form>
    </DetailLayout>
  )
}

SatisfactionRating.propTypes = {
  t: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
}

export default withTranslation()(SatisfactionRating)
