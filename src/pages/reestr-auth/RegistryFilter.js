import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { always } from 'ramda'
import { Search } from 'react-feather'
import * as API from 'constants/api'
import {
  Form,
  Field,
  AutoSave,
  IconInputField,
  UniversalMultiSelectField,
  UniversalStaticMultiSelectField
} from 'components/FormComponents'
import { FieldWrap } from 'components/StyledElements'
import { ButtonBordered } from 'components/Button'
import { RegistryFilterWrapper } from 'components/RegistryAuth'
import { registryStatus } from 'constants/backend'

const RegistryFilter = props => {
  const {
    t,
    initialValues,
    onFormChange,
    onFormReset
  } = props

  const onEnterSearch = (event, fieldValue) => {
    const fieldName = event.target.name

    event.preventDefault()

    onFormChange({ [fieldName]: fieldValue }, true)
  }

  return (
    <RegistryFilterWrapper>
      <Form initialValues={initialValues} onSubmit={always(true)}>
        <Fragment>
          <AutoSave
            debounce={300}
            onFormChange={onFormChange}
          />

          <FieldWrap>
            <ButtonBordered fullWidth={true} onClick={onFormReset}>
              {t('registry_confirm_reset_form')}
            </ButtonBordered>
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'search'}
              component={IconInputField}
              label={'Kalit so’zlar'}
              placeholder={t('search')}
              icon={Search}
              onEnter={onEnterSearch}
            />
          </FieldWrap>

        </Fragment>
      </Form>
    </RegistryFilterWrapper>
  )
}

RegistryFilter.propTypes = {
  t: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  onFormChange: PropTypes.func.isRequired,
  onFormReset: PropTypes.func.isRequired,
  regionList: PropTypes.array.isRequired,
  organTypeList: PropTypes.array.isRequired,
}

export default RegistryFilter
