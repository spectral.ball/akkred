import React from 'react'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import Inspection from './Inspection'

const InspectionContainer = props => (
  <Inspection {...props} />
)

InspectionContainer.getInitialProps = async ({ query }) => {
  const params = { pageSize: 1000, year:query.year }
  const data = await fetchData(API.INSPECTION_LIST, params)

  return {
    data,
    namespacesRequired: ['common']
  }
}

export default InspectionContainer
