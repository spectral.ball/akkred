import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import * as ROUTES from 'constants/routes'
import { withTranslation } from 'hocs/withTranslation'
import AppContext from 'components/Contexts/AppContext'
import { TitleMore } from 'components/Title'
import { NewsGrid, NewsCard } from 'components/News'
import { BackgroundFull } from 'components/StyledElements'
import { slice } from 'ramda'

const Wrapper = styled(BackgroundFull)`
   padding: 50px 0;
`

const NewsBlock = props => {
  const { t } = props

  const { news } = useContext(AppContext)

  const newsList = slice(0, 3, news)

  return (
    <Wrapper>
      <TitleMore
        title={t('news_title')}
        linkText={t('news_all')}
        href={ROUTES.NEWS_PATH}
      />

      <NewsGrid>
        {newsList.map(item =>
          <NewsCard key={item.id} data={item} />
        )}
      </NewsGrid>
    </Wrapper>
  )
}

NewsBlock.propTypes = {
  t: PropTypes.func.isRequired
}

export default withTranslation()(NewsBlock)
