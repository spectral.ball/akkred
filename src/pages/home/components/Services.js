import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import * as ROUTES from 'constants/routes'
import { devices } from 'constants/mediaQueries'
import { withTranslation } from 'hocs/withTranslation'
import { TitleMore } from 'components/Title'
import Link from 'components/Link'
import Account from '../icons/Account'
import Application from '../icons/Application'
import Search from '../icons/Search'
import Training from '../icons/Training'

const Wrapper = styled('div')`
  margin: 50px 0;
`

const Content = styled('div')`
  display: grid;
  grid-gap: 48px;
  @media (min-width: ${devices.tabletL}) {
    grid-template-columns: repeat(2, 1fr);
  }
`

const Service = styled('div')`
  background-color: white;
  border-radius: 5px;
  cursor: pointer;
  min-height: 220px;
  padding: 20px 30px 20px 20px;
  position: relative;
  transition: ${props => props.theme.transition};
  &:hover {
    box-shadow: 0 4px 35px rgba(0, 0, 0, 0.15);
  }
`

const Title = styled('div')`
  font-size: 20px;
  margin-bottom: 15px;
  width: calc(100% - 80px);
`

const Description = styled('div')`
  line-height: 20px;
  width: calc(100% - 120px);
`

const IconWrapper = styled('div')`
  align-items: center;
  background-color: ${props => props.theme.colors.primary};
  border-radius: 20px;
  display: flex;
  justify-content: center;
  height: 60px;
  padding: 10px;
  position: absolute;
  top: 30px;
  right: 30px;
  width: 60px;
  & > svg {
    color: white;
    font-size: 35px;
  }
`

const FakeLink = styled('span')`
  font-weight: normal;
  position: absolute;
  bottom: 20px;
  left: 20px;
  transition: ${props => props.theme.transition};
`

const AbsoluteLink = styled(Link)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  &:hover + ${FakeLink} {
    color: ${props => props.theme.colors.primary};
  }
`

const data = [
  {
    title: 'services_registry_search',
    description: 'services_registry_search_desc',
    link: ROUTES.REGISTRY_PATH,
    icon: <Search />
  },
  {
    title: 'tnv_static_title',
    description: 'tnv_static_title',
    link: '/code-tnv',
    icon: <Account />
  },
  {
    title: 'calculate_title',
    description: 'calculate_title',
    link: ROUTES.CALCULATE_PATH,
    icon: <Training />
  },
  {
    title: 'services_online_submission',
    description: 'services_online_submission_desc',
    link: ROUTES.CONTACTS_PATH,
    icon: <Application />
  },
]

const Services = props => {
  const { t } = props

  return (
    <Wrapper>
      <TitleMore
        title={t('services_title')}
        linkText={t('services_all')}
        href={'/'}
      />

      <Content>
        {data.map((item, index) => {
          const title = prop('title', item)
          const description = prop('description', item)
          const icon = prop('icon', item)
          const link = prop('link', item)

          return (
            <Service key={index}>
              <Title>{t(title)}</Title>
              <Description>{t(description)}</Description>

              <IconWrapper>{icon}</IconWrapper>
              <AbsoluteLink href={link} />
              <FakeLink>{t('go_to')}</FakeLink>
            </Service>
          )
        })}
      </Content>
    </Wrapper>
  )
}

Services.propTypes = {
  t: PropTypes.func.isRequired
}

export default withTranslation()(Services)
