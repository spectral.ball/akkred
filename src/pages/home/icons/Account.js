/* eslint-disable */
import React from 'react'
import SVG from 'components/Svg'

export default props => {
  return (
    <SVG viewBox={'0 0 35 35'} {...props}>
      <g>
        <path d="M30.9,0H4.1C1.8,0,0,1.8,0,4.1v5.5h2.7V4.1c0-0.8,0.6-1.4,1.4-1.4h26.8c0.8,0,1.4,0.6,1.4,1.4v26.8c0,0.8-0.6,1.4-1.4,1.4 H4.1c-0.8,0-1.4-0.6-1.4-1.4v-5.5H0v5.5C0,33.2,1.8,35,4.1,35h26.8c2.3,0,4.1-1.8,4.1-4.1V4.1C35,1.8,33.2,0,30.9,0z"/>
        <path d="M16.4,9l-1.9,1.9l5.2,5.2H0v2.7h19.7l-5.2,5.2l1.9,1.9l8.5-8.5L16.4,9z"/>
      </g>
    </SVG>
  )
}
