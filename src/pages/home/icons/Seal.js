/* eslint-disable */
import React from 'react'
import SVG from 'components/Svg'

export default props => {
  return (
    <SVG viewBox={'0 0 45 45'} {...props}>
      <g>
        <path d="M27.5,18.3l-6.8,6.4l-3.2-3.3c-0.4-0.4-1-0.4-1.3,0c-0.4,0.4-0.4,1,0,1.3l3.9,4c0.4,0.4,0.9,0.4,1.3,0l7.4-7 c0.2-0.2,0.3-0.4,0.3-0.7c0-0.2-0.1-0.5-0.3-0.7C28.5,18,27.9,18,27.5,18.3z"/>
        <path d="M22.5,8.5c-7.7,0-14,6.3-14,14c0,7.7,6.3,14,14,14c7.7,0,14-6.3,14-14C36.5,14.8,30.2,8.5,22.5,8.5z M22.5,34.6 c-6.7,0-12.1-5.4-12.1-12.1c0-6.7,5.4-12.1,12.1-12.1c6.7,0,12.1,5.4,12.1,12.1C34.6,29.2,29.2,34.6,22.5,34.6z"/>
        <path d="M41.8,17.3l0.3-5.6c0-0.4-0.2-0.7-0.5-0.9l-5-2.5l-2.5-5c-0.2-0.3-0.5-0.5-0.9-0.5l-5.6,0.3L23,0.2c-0.3-0.2-0.7-0.2-1,0 l-4.6,3.1l-5.6-0.3c-0.4,0-0.7,0.2-0.9,0.5l-2.5,5l-5,2.5c-0.3,0.2-0.5,0.5-0.5,0.9l0.3,5.6L0.2,22c-0.2,0.3-0.2,0.7,0,1l3.1,4.7 l-0.3,5.6c0,0.4,0.2,0.7,0.5,0.9l5,2.5l2.5,5c0.2,0.3,0.5,0.5,0.9,0.5l5.6-0.3l4.6,3.1c0.3,0.2,0.7,0.2,1,0l4.6-3.1l5.6,0.3 c0.4,0,0.7-0.2,0.9-0.5l2.5-5l5-2.5c0.3-0.2,0.5-0.5,0.5-0.9l-0.3-5.6l3.1-4.6c0.2-0.3,0.2-0.7,0-1L41.8,17.3z M40,26.9 c-0.1,0.2-0.2,0.4-0.1,0.6l0.3,5.3l-4.7,2.4c-0.2,0.1-0.3,0.2-0.4,0.4l-2.4,4.7l-5.3-0.3c-0.2,0-0.4,0-0.6,0.1l-4.4,2.9L18.1,40 c-0.2-0.1-0.3-0.2-0.5-0.2h0l-5.3,0.3l-2.4-4.7c-0.1-0.2-0.2-0.3-0.4-0.4l-4.7-2.4l0.3-5.3c0-0.2,0-0.4-0.2-0.6l-2.9-4.4L5,18.1 c0.1-0.2,0.2-0.4,0.2-0.6l-0.3-5.3l4.7-2.4c0.2-0.1,0.3-0.2,0.4-0.4l2.4-4.7l5.3,0.3c0.2,0,0.4,0,0.6-0.2l4.4-2.9L26.9,5 c0.2,0.1,0.4,0.2,0.6,0.2l5.3-0.3l2.4,4.7c0.1,0.2,0.2,0.3,0.4,0.4l4.7,2.4l-0.3,5.3c0,0.2,0,0.4,0.1,0.6l2.9,4.4L40,26.9z"/>
      </g>
    </SVG>
  )
}
