import React, { useState } from 'react'
import { not, path, pipe } from 'ramda'
import { useRouter } from 'next/router'
import { useToasts } from 'react-toast-notifications'
import * as API from 'constants/api'
import * as ROUTES from 'constants/routes'
import { mapResponseToFormError } from 'helpers/form'
import scrollToTop from 'helpers/scrollToTop'
import SatisfactionRating from './SatisfactionRating'

const serializer = values => ({
  ...values,
  type_of_activity: path(['type_of_activity', 'id'], values),
  questionnaire: path(['questionnaire', 'id'], values)
})

const SatisfactionRatingContainer = props => {
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const { addToast } = useToasts()

  async function onSubmit (values) {
    setLoading(true)

    const body = pipe(serializer, JSON.stringify)(values)
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body
    }
    const data = await fetch(API.SATISFY_RATING_CREATE, options)
      .then(response => {
        setLoading(false)

        if (not(response.ok)) {
          return Promise.reject(response.json())
        }

        return response.json()
      })
      .then(() => {
        addToast('Успешно отправлено', {
          appearance: 'success'
        })
        router.replace(ROUTES.ROOT_PATH)
      })
      .catch(response => {
        addToast('Ошибка', {
          appearance: 'error'
        })
        scrollToTop()
        return response.then(mapResponseToFormError)
      })

    return data
  }

  return (
    <SatisfactionRating
      onSubmit={onSubmit}
      loading={loading}
      {...props}
    />
  )
}

SatisfactionRatingContainer.getInitialProps = async () => ({
  namespacesRequired: ['common']
})

export default SatisfactionRatingContainer
