import React from 'react'
import Inspection from './Inspection'

const InspectionContainer = props => (
  <Inspection {...props} />
)
export default InspectionContainer
