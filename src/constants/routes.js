export const ROOT_PATH = '/'

const NEWS = 'news'
export const NEWS_PATH = `/${NEWS}`
export const NEWS_ITEM_PATH = `/${NEWS}/[id]`
export const NEWS_ITEM_URL = `/${NEWS}/%d`

const DOCUMENTS = 'documents'
export const DOCUMENTS_PATH = `/${DOCUMENTS}`
export const DOCUMENTS_ITEM_PATH = `/${DOCUMENTS}/[id]`
export const DOCUMENTS_ITEM_URL = `/${DOCUMENTS}/%d`

const REGISTRY = 'reestr'
export const REGISTRY_PATH = `/${REGISTRY}`
export const REGISTRY_ITEM_PATH = `/${REGISTRY}/[id]`
export const REGISTRY_ITEM_URL = `/${REGISTRY}/%s`

const REGISTRY_CONFIRM = 'reestr-confirm'
export const REGISTRY_CONFIRM_PATH = `/${REGISTRY_CONFIRM}`
export const REGISTRY_CONFIRM_ITEM_PATH = `/${REGISTRY_CONFIRM}/[id]`
export const REGISTRY_CONFIRM_ITEM_URL = `/${REGISTRY_CONFIRM}/%s`

const CONTACTS = 'contacts'
export const CONTACTS_PATH = `/${CONTACTS}`

const SATISFACTION_RATING = 'satisfaction-rating'
export const SATISFACTION_RATING_PATH = `/${SATISFACTION_RATING}`

const MANAGEMENT = 'management'
export const MANAGEMENT_PATH = `/${MANAGEMENT}`

const CALCULATE = 'calculate'
export const CALCULATE_PATH = `/${CALCULATE}`
export const CALCULATE_ITEM_PATH = `/${CALCULATE}/[service]`
export const CALCULATE_ITEM_URL = `/${CALCULATE}/%s`
