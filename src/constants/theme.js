import hexToRgb from '../helpers/hexToRgb'

const colors = {
  border: '#e0e0e0',
  blue: '#1844f7',
  green: '#219653',
  grey: '#f7f7f7',
  grey2: '#4f4f4f',
  grey3: '#f2f2f2',
  grey4: '#828282',
  primary: '#0a2cb7',
  red: '#dd2a1b',
  text: '#333333',
  yellow: '#f2994a'
}

const input = {
  backgroundColor: '#ffffff',
  backgroundColorHover: colors.grey,
  placeholderColor: '#bec7d1'
}

export default {
  colors,
  input,
  border: '1px solid #e0e0e0',
  borderLight: '1px solid ' + hexToRgb('#c6cbd4', '0.3'),
  borderRadius: '5px',
  boxShadow: '0 4px 15px rgba(0, 0, 0, 0.15)',
  transition: 'all 300ms ease'
}
